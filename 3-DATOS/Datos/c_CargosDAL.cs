﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos {
    public class CargosDAL {
        public ResultadoDTO CrearCargo(cargos dto) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.cargos.Add(dto);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = dto.idCargo;
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public List<ItemCargos> ListarCargos() {
            List<ItemCargos> lista = new List<ItemCargos>();
            using (nomeEntities dbo = new nomeEntities()) {
                lista = (from l in dbo.cargos
                         select new ItemCargos { idCargo= l.idCargo, descrCargo=l.descrCargo}).ToList();
            }
            return lista;
        }

        public ResultadoDTO ModificarCargos(int idCargo, String descrCargo) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    cargos objTemp = dbo.cargos.FirstOrDefault(u => u.idCargo == idCargo);
                    if (objTemp != null) {
                        if ((descrCargo != null) && (descrCargo != "")) { objTemp.descrCargo = descrCargo; }
                        dbo.SaveChanges();
                        r.error = false;
                        r.id = idCargo;
                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public cargos BuscarCargos(int idCargo) {
            cargos objTemp = new cargos();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objTemp = dbo.cargos.FirstOrDefault(u => u.idCargo == idCargo);
                }
            } catch  {
                objTemp = null;
            }
            return objTemp;
        }

        public ResultadoDTO EliminarCargo(int idCargo) {
            ResultadoDTO r = new ResultadoDTO();
            cargos objCargo = BuscarCargos(idCargo);
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.cargos.Remove(objCargo);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = objCargo.idCargo;
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }
    }
}
