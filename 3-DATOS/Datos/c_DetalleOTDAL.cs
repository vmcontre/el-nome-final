﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace Datos {
    public class DetalleOTDAL {
        public ResultadoDTO CrearDetalleOT(detalleOT dto) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.detalleOT.Add(dto);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = dto.idOT;
                    RepuestosDAL objTempRep = new RepuestosDAL();
                    ResultadoDTO rr = objTempRep.RebajarStock(dto.idRepuesto);
                    
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public List<ItemDetalleOT> ListarDOT(int id) {
            List<ItemDetalleOT> lista = new List<ItemDetalleOT>();

            using (nomeEntities dbo = new nomeEntities()) {
                var q = (from i in dbo.detalleOT where i.idOT == id select i).ToList();
                if (q != null) {
                    foreach (var qq in q) {
                        ItemDetalleOT objTemp = new ItemDetalleOT() {
                            idDOT = qq.idDOT,
                            idOT = qq.idOT,
                            idTecnico = qq.idTecnico,
                            idRepuesto = qq.idRepuesto,
                            idFalla = qq.idFalla,
                            fecha = qq.fecha,
                            
                        };
                        lista.Add(objTemp);
                    }
                }
            } return lista;
        }

        public detalleOT BuscarDetOT(int id) { //quizás haya que cambiarla por itemDOT
            detalleOT objDet = new detalleOT();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objDet = dbo.detalleOT.FirstOrDefault(u => u.idDOT==id);
                }
            } catch {
                objDet = null;
            }
            return objDet;
        }

        public ResultadoDTO ModificarDetOT(int id, int idTec, int idRep, int idFalla) {
            ResultadoDTO r = new ResultadoDTO();
            detalleOT objDet = new detalleOT();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objDet = dbo.detalleOT.FirstOrDefault(u => u.idDOT == id);
                    if (objDet != null) {

                        if (idTec > 0) { objDet.idTecnico = idTec; }
                        if (idRep > 0) { objDet.idRepuesto = idRep; }
                        if (idFalla > 0) { objDet.idFalla = idFalla; }
                        objDet.fecha = DateTime.Now;
                        dbo.SaveChanges();
                    }
                }                
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }
        
        public DetalleOTDTO BuscarDetalleOT_DTO(int id) {
            DetalleOTDTO objDetOT = new DetalleOTDTO();
            using (nomeEntities dbo = new nomeEntities()) {
                objDetOT = (from o in dbo.detalleOT where o.idDOT == id
                            select new DetalleOTDTO {
                                idDOT = o.idDOT,
                                idOT = o.idOT,
                                fecha = o.fecha,
                                nombreFalla = o.fallas.descrFalla,
                                nombreTecnico = o.personas.nombrePersona,
                                nombreRepuesto = o.repuestos.descrRepuesto
                            }).FirstOrDefault();
            }
            return objDetOT;
        }

        public List<DetalleOTDTO> ListarDOT_DTO(int id) {
            List<DetalleOTDTO> lista = new List<DetalleOTDTO>();

            using (nomeEntities dbo = new nomeEntities()) {
                var q = (from i in dbo.detalleOT where i.idOT == id select i).ToList();
                if (q != null) {
                    foreach (var qq in q) {
                        DetalleOTDTO objTemp = new DetalleOTDTO() {
                            idDOT = qq.idDOT,
                            idOT = qq.idOT,
                            fecha = qq.fecha,
                            nombreFalla = qq.fallas.descrFalla,
                            nombreTecnico = qq.personas.nombrePersona,
                            nombreRepuesto = qq.repuestos.descrRepuesto
                            
                        };
                        lista.Add(objTemp);
                    }
                }
            }
            return lista;
        }
    }
}
