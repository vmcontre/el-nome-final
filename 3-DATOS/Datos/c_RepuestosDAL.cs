﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace Datos {
    public class RepuestosDAL {
        public ResultadoDTO CrearRepuesto(repuestos dto) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.repuestos.Add(dto);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = dto.idRepuesto;
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public List<ItemRepuestos> ListarRepuestos() {
            List<ItemRepuestos> lista = new List<ItemRepuestos>();
            using (nomeEntities dbo = new nomeEntities()) {
                lista = (from l in dbo.repuestos select new ItemRepuestos {idRepuesto=l.idRepuesto, descrRepuesto = l.descrRepuesto, stockRepuesto=l.stockRepuesto }).ToList();
            }
            return lista;
        }
        public List<ItemRepuestos> ListarRepuestosConStock() {
            List<ItemRepuestos> lista = new List<ItemRepuestos>();
            using (nomeEntities dbo = new nomeEntities()) {
                lista = (from l in dbo.repuestos where l.stockRepuesto > 0  select new ItemRepuestos { idRepuesto = l.idRepuesto, descrRepuesto = l.descrRepuesto, stockRepuesto = l.stockRepuesto }).ToList();
            }
            return lista;
        }

        public repuestos BuscarRepuesto(int id) {
            repuestos objRepuesto = new repuestos();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objRepuesto = dbo.repuestos.FirstOrDefault(u => u.idRepuesto == id);
                }
            } catch  {
                objRepuesto = null;
            }
            return objRepuesto;
        }

        public ResultadoDTO ActualizarStockRepuesto(int idRepuesto, int stockRepuesto, String nuevoNombreRep) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    repuestos objTemp = dbo.repuestos.FirstOrDefault(u => u.idRepuesto == idRepuesto);
                    if (objTemp!=null) {
                        r.error = true;
                        r.mensaje = "algo no funcionó";
                        if (stockRepuesto >= 0) {
                            objTemp.stockRepuesto = stockRepuesto;
                            r.error = false;
                            r.mensaje = "";
                        }
                        if (!nuevoNombreRep.Equals("")) {
                            objTemp.descrRepuesto = nuevoNombreRep;
                            r.error = false;
                            r.mensaje = "";
                        }                        
                        dbo.SaveChanges();
                        r.id = idRepuesto;

                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public ResultadoDTO BorrarRepuesto(int idRep) {
            ResultadoDTO r = new ResultadoDTO();
            try {
               // repuestos temp = new repuestos();
                using (nomeEntities dbo = new nomeEntities()) {
                    repuestos temp = dbo.repuestos.FirstOrDefault(b => b.idRepuesto == idRep);
                    if (temp != null) {
                        dbo.repuestos.Remove(temp);
                        dbo.SaveChanges();
                        r.error = false;
                        r.id = temp.idRepuesto;
                    } else {
                        r.error = true;
                        r.mensaje = "no pude borrar";
                    }                 
                }

            } catch {
                r.error = true;
                r.mensaje = "No puedo borrar esto, está en uso";
            }
            return r;
        }

        public ResultadoDTO RebajarStock(int idRep) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    repuestos temp = dbo.repuestos.FirstOrDefault(b => b.idRepuesto == idRep);
                    if (temp != null) {
                        temp.stockRepuesto = temp.stockRepuesto - 1;
                        dbo.SaveChanges();
                        r.error = false;
                        r.id = temp.idRepuesto;
                    } else {
                        r.error = true;
                        r.mensaje = "no pude rebajar stock";
                    }
                }
            } catch  {
                r.mensaje = "no pude rebajar stock";
            }
            return r;
        }

    }
}
