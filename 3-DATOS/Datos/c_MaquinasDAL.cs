﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace Datos {
    public class MaquinasDAL {
        public ResultadoDTO CrearMaquina(maquinas dto) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.maquinas.Add(dto);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = dto.idMaquina;
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            } return r;
        }

        public List<ItemMaquinas> ListarMaquinas() {
            List<ItemMaquinas> lista = new List<ItemMaquinas>();
            using (nomeEntities dbo = new nomeEntities()) {
                lista = (from l in dbo.maquinas select new ItemMaquinas { idMaquina = l.idMaquina, descrMaquina = l.descrMaquina, fechaCompra = l.fechaCompra, fechaFinGtia = l.fechaFinGarantia, activo = l.activo}).ToList(); 
            }
            return lista;
        }

        public ResultadoDTO ModificarMaquina(int idMaq, String nvaDescr, DateTime? nuevaFechaGtia) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    maquinas objTemp = dbo.maquinas.FirstOrDefault(u => u.idMaquina == idMaq);
                    if (objTemp != null) {
                        if ((nvaDescr != null) && (nvaDescr != "")){ objTemp.descrMaquina = nvaDescr; }
                        if (nuevaFechaGtia != null) { objTemp.fechaFinGarantia = nuevaFechaGtia; }

                        dbo.SaveChanges();
                        r.error = false;
                        r.id = idMaq;

                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public ResultadoDTO CambiaEstadoMaquina(int idMaq) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    maquinas objTemp = dbo.maquinas.FirstOrDefault(u => u.idMaquina == idMaq);
                    if (objTemp != null) {
                        objTemp.activo = !objTemp.activo;
                        dbo.SaveChanges();
                        r.error = false;
                        r.id = idMaq;

                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public ItemMaquinas BuscarMaquina(int idMaq) {
            ItemMaquinas objMaq = new ItemMaquinas();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    maquinas Maq = dbo.maquinas.FirstOrDefault(m => m.idMaquina == idMaq);
                    if (Maq != null) {
                        objMaq.idMaquina = Maq.idMaquina;
                        objMaq.fechaFinGtia = Maq.fechaFinGarantia;
                        objMaq.fechaCompra = Maq.fechaCompra;
                        objMaq.descrMaquina = Maq.descrMaquina;
                        objMaq.activo = Maq.activo;
                    } else {
                        objMaq = null;
                    }
                }
                return objMaq;
            } catch {
                return null;
            }
            
        }


    }
}
