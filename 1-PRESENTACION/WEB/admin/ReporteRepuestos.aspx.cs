﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.admin {
    public partial class ReporteRepuestos : System.Web.UI.Page {
        SRV_Repuestos.SRV_RepuestosClient repuestosClient = new SRV_Repuestos.SRV_RepuestosClient();
        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                refrescador();
            }
        }

        protected void gvRepuestos_RowDataBound(Object sender, GridViewRowEventArgs e) {

        }

        protected void gvRepuestos_RowCreated(Object sender, GridViewRowEventArgs e) {

        }
        protected void refrescador() {
            //mensajes
            lblMensaje.Text = "";
            lblResultadoAgregar.Text = "";
            //txtNuevoRepuesto.Text = "";
            //txtNuevoStock.Text = "";
            //grilla
            gvRepuestos.DataSource = repuestosClient.ListarRepuestos();
            gvRepuestos.DataBind();
        }

        protected void gvRepuestos_RowCancelingEdit(Object sender, GridViewCancelEditEventArgs e) {
            gvRepuestos.EditIndex = -1;
            refrescador();
        }

        protected void gvRepuestos_RowEditing(Object sender, GridViewEditEventArgs e) {
            gvRepuestos.EditIndex = e.NewEditIndex;
            refrescador();
        }

        protected void gvRepuestos_RowDeleting(Object sender, GridViewDeleteEventArgs e) {

            int id = Convert.ToInt32(gvRepuestos.DataKeys[e.RowIndex].Values["idRepuesto"].ToString());
            var r = repuestosClient.BorrarRepuesto(id);
            if (!r.error) {
                refrescador();
                lblMensaje.Text = "Repuesto borrado con éxito";
            } else {
                lblMensaje.Text = r.mensaje;
            }
        }

        protected void gvRepuestos_RowUpdating(Object sender, GridViewUpdateEventArgs e) {

            //no está leyendo el valor nuevo desde el textbox. Después lo corrijo
            TextBox txtNvaDescr = gvRepuestos.Rows[e.RowIndex].FindControl("txtDescrRepuesto") as TextBox;
            TextBox txtNvoStock = gvRepuestos.Rows[e.RowIndex].FindControl("txtstockRepuesto") as TextBox;
            int id = Convert.ToInt32(gvRepuestos.DataKeys[e.RowIndex].Values["idRepuesto"].ToString());

            //actualizar
            try {
                var r = repuestosClient.ActualizarStockRepuesto(id, Convert.ToInt32(txtNvoStock.Text), txtNvaDescr.Text);
                if (!r.error) {
                    refrescador();
                    lblMensaje.Text = "Repuesto actualizado con éxito";
                } else {
                    lblMensaje.Text = r.mensaje;
                }

                gvRepuestos.EditIndex = -1;
                refrescador();
            } catch {
                lblMensaje.Text = "verifique formato numérico en stock";
            }
            
        }

        protected void btnAgregarRepuesto_Click(Object sender, EventArgs e) {
            String descripcionNuevoRepuesto = txtNuevoRepuesto.Text.Trim();
            try {
                int stockNuevoRepuesto = Int32.Parse(txtNuevoStock.Text.Trim());
                if ((!descripcionNuevoRepuesto.Equals("")) && (stockNuevoRepuesto >= 0)) {
                    var r = repuestosClient.CrearRepuesto(descripcionNuevoRepuesto, stockNuevoRepuesto);
                    if (!r.error) {
                        refrescador();
                        txtNuevoRepuesto.Text = "";
                        txtNuevoStock.Text = "";
                        lblResultadoAgregar.Text = "Repuesto agregado correctamente";

                    }
                } else {
                    refrescador();
                    lblResultadoAgregar.Text = "no se pudo guardar, faltan datos";
                }
            } catch {
                lblResultadoAgregar.Text = "Verifique formato numérico en stock";
            }
        }
    }
}