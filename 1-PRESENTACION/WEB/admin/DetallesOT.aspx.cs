﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.admin {
    public partial class DetallesOT : System.Web.UI.Page {
        SRV_DetOT.SRV_DetOTClient detOTClient = new SRV_DetOT.SRV_DetOTClient();
        SRV_OrdenTrabajo.SRV_OrdenTrabajoClient ordenTrabajoClient = new SRV_OrdenTrabajo.SRV_OrdenTrabajoClient();
        SRV_StatusOT.SRV_StatusOTClient statusOTClient = new SRV_StatusOT.SRV_StatusOTClient();
        SRV_Personas.SRV_PersonasClient personasClient = new SRV_Personas.SRV_PersonasClient();
        SRV_Fallas.SRV_FallasClient fallasClient = new SRV_Fallas.SRV_FallasClient();
        SRV_Repuestos.SRV_RepuestosClient repuestosClient = new SRV_Repuestos.SRV_RepuestosClient();

        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                refrescador();
            }
            
        }

        protected void gvDetalleOT_RowDataBound(Object sender, GridViewRowEventArgs e) {
            e.Row.Attributes.Add("onclick", "$('#" + hdfId.ClientID + "').val('" + e.Row.Cells[0].Text + "'); $('#" + btnDetalles.ClientID + "').click();");
            hdfId.Value = e.Row.Cells[0].Text;
        }

        protected void gvDetalleOT_RowCreated(Object sender, GridViewRowEventArgs e) {
            e.Row.Cells[0].Visible = false;
        }

        protected void btnDetalles_Click(Object sender, EventArgs e) {

        }

        protected void btnCerrarOT_Click(Object sender, EventArgs e) {
            int idOT, idEstadoOT;
            try {
                idOT = Convert.ToInt32(Session["SesionDetalles"]);
                idEstadoOT = Int32.Parse(ddlEstadoOT.SelectedValue);
                var r = ordenTrabajoClient.CambiarEstadoOT(idOT, idEstadoOT);
                if (!r.error) {
                    refrescador();
                    lblMensaje.Text = "Estado cambiado exitosamente";
                } else {
                    lblMensaje.Text = r.mensaje;
                }
            } catch {
                lblMensaje.Text = "Error de formato no numérico";
            }
           
        }

        protected void btnVolver_Click(Object sender, EventArgs e) {
            Response.Redirect("~/admin/index.aspx");
        }
        protected Boolean validador() {
           
            if (ddlTecnico.Text.Equals("")) return false;
            if (ddlFalla.Text.Equals("")) return false;
            if (ddlRepuesto.Text.Equals("")) {
                return false;
            } else {
                return true;
            }
        }

        protected void btnAgregarDOT_Click(Object sender, EventArgs e) {
            if (validador()) {
                try {
                    var r = detOTClient.CrearDetalleOT(
                          Convert.ToInt32(Session["SesionDetalles"]),
                          Int32.Parse(ddlTecnico.SelectedValue),
                          Int32.Parse(ddlRepuesto.SelectedValue),
                          Int32.Parse(ddlFalla.SelectedValue));
                    if (!r.error) {
                        refrescador();
                        lblMensaje.Text = "Detalle agregado correctamente";
                    } else {
                        lblMensaje.Text = r.mensaje;
                    }
                } catch {
                    lblMensaje.Text = "Error de formato no numérico ¿seleccionó apropiadamente desde las listas desplegables?";
                }

            }
        }
        protected void refrescador() {
            try {
                Int32 SesionidOT = Convert.ToInt32(Session["SesionDetalles"]);
                SRV_OrdenTrabajo.OrdenTrabajoSRV objOrdenTrabajo = ordenTrabajoClient.BuscarOrdenTrabajo(SesionidOT);
                lblCreador2.Text = objOrdenTrabajo.nombreCreador;
                lblEncargado2.Text = objOrdenTrabajo.nombreEncargado;
                lblFechaCreacion2.Text = objOrdenTrabajo.fechaCreacion.ToString();
                lblEstado2.Text = objOrdenTrabajo.estadoOT;

                gvDetalleOT.DataSource = detOTClient.ListarDOT(SesionidOT);
                gvDetalleOT.DataBind();
                ddlEstadoOT.DataSource = statusOTClient.listarStatusOT();
                ddlEstadoOT.DataTextField = "DescrStatus";
                ddlEstadoOT.DataValueField = "idStatusOT";
                ddlEstadoOT.DataBind();
                ddlEstadoOT.Items.Insert(0, new ListItem(""));

                btnDetalles.Visible = false;
                ddlTecnico.DataSource = personasClient.ListarPersonasActivas();
                ddlTecnico.DataTextField = "nombrePersona";
                ddlTecnico.DataValueField = "idPersona";
                ddlTecnico.DataBind();
                ddlTecnico.Items.Insert(0, new ListItem(""));

                //Fallas
                ddlFalla.DataSource = fallasClient.ListarFallas();
                ddlFalla.DataTextField = "descrFalla";
                ddlFalla.DataValueField = "idFalla";
                ddlFalla.DataBind();
                ddlFalla.Items.Insert(0, new ListItem(""));

                //Repuestos
                ddlRepuesto.DataSource = repuestosClient.ListarRepuestosConStock();
                ddlRepuesto.DataTextField = "descrRepuesto";
                ddlRepuesto.DataValueField = "idRepuesto";
                ddlRepuesto.DataBind();
                ddlRepuesto.Items.Insert(0, new ListItem(""));

                //si el estado = 4 (finalizada) o 5 (cancelada) deshabilitar modificadores
                int estado = ordenTrabajoClient.BuscarOrdenTrabajo(SesionidOT).idStatusOT;
                if ((estado == 4) || (estado == 5)) {
                    btnAgregarDOT.Enabled = false;
                    btnCerrarOT.Enabled = false;
                } else {
                    btnAgregarDOT.Enabled = true;
                    btnCerrarOT.Enabled = true;
                }
            } catch {
                //algo le pasó a los datos de la sesión, vuelta pa'tras
                Session["SesionDetalles"] = null;
                Response.Redirect("~/admin/index.aspx");
            }
            

           
        }
    }
}