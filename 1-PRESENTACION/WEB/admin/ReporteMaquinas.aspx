﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteMaquinas.aspx.cs" Inherits="WEB.admin.ReporteMaquinas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-body">
        <!--#include file="MenuLateral.html"-->
        <main class="overflow-hidden">
            <header class="page header">
                <div class="content">
                    <h1 class="display-4 mb-0">Listado Máquinas</h1>
                    <p class="lead text-muted">Detalles de máquinas</p>
                    <asp:HiddenField runat="server" ID="hdfId" />
                    <asp:HiddenField runat="server" ID="hdfModificar" />
                    <asp:HiddenField runat="server" ID="hdfId2" />
                </div>
            </header>
            <div class="content">
                <div class="table-responsive">
                    <asp:GridView ID="gvMaquinas" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-clean" OnRowCreated="gvMaquinas_RowCreated" OnRowDataBound="gvMaquinas_RowDataBound">
                        <HeaderStyle CssClass="table-clean" />
                        <Columns>
                            <asp:BoundField DataField="idMaquina" HeaderText="ID Maquina" />
                            <asp:BoundField DataField="descrMaquina" HeaderText="Descripcion Maquina" />
                            <asp:BoundField DataField="fechaCompra" HeaderText="Fecha Compra"  DataFormatString="{0:d}" />
                            <asp:BoundField DataField="fechaFinGarantia" HeaderText="Fecha Fin Garantia" DataFormatString="{0:d}"/>
                            <asp:BoundField DataField="activo" HeaderText="Activo" />
                        </Columns>
                    </asp:GridView>

                </div>
            </div>

            <div class="content">
                <p class="lead text-muted">Agregar nueva maquina</p>
                <p class="lead text-muted">
                    <asp:Label ID="lblResultadoAgregar" runat="server" Text=""></asp:Label>
                </p>
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblNuevaMaquina" runat="server" Text="Descripción:" CssClass="form-label"></asp:Label>
                        <asp:TextBox ID="txtNuevaMaquina" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <asp:Label ID="lblFechaCompra" runat="server" Text="Fecha Compra:" CssClass="form-label"></asp:Label>
                        <asp:TextBox ID="txtFechaCompra" runat="server"  CssClass="form-control" TextMode="Date"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblFechaGarantia" runat="server" Text="Fecha Vencimiento Garantía:" CssClass="form-label"></asp:Label>
                        <asp:TextBox ID="txtFechaGarantia" runat="server"  CssClass="form-control" TextMode="Date"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <br />
                        <br />
                        <asp:Button ID="btnAgregarMaquina" runat="server" Text="Agregar" CssClass="btn btn-primary" OnClick="btnAgregarMaquina_Click" />
                    </div>
                    <div class="col-md-4">
                        <br />
                        <br />
                        <asp:Button ID="btnCambiarEstado" runat="server" Text="Cambiar Estado" CssClass="btn btn-primary" OnClick="btnCambiarEstado_Click" />
                    </div>
                </div>
            </div>
        </main>
    </div>

</asp:Content>
