﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.admin {
    public partial class ReporteMaquinas : System.Web.UI.Page {
        SRV_Maquinas.SRV_MaquinasClient maquinasClient = new SRV_Maquinas.SRV_MaquinasClient();
        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                refrescador();
            }
        }

        protected void gvMaquinas_RowDataBound(Object sender, GridViewRowEventArgs e) {
            if (e.Row.RowType != DataControlRowType.Header) {
                e.Row.Attributes.Add("onclick", "$('#" + hdfId.ClientID + "').val('" + e.Row.Cells[0].Text + "'); $('#" + btnAgregarMaquina.ClientID + "').click();");
                
            }
        }

        protected void gvMaquinas_RowCreated(Object sender, GridViewRowEventArgs e) {
            e.Row.Cells[0].Visible = false;
        }

        protected void refrescador() {
            //txts
            lblResultadoAgregar.Text = "";
            txtNuevaMaquina.Text = "";
            txtFechaGarantia.Text = "";
            txtFechaCompra.Text = "";
            btnCambiarEstado.Enabled = false;
            btnAgregarMaquina.Text = "Agregar";

            //grilla
            gvMaquinas.DataSource = maquinasClient.ListarMaquinas();
            gvMaquinas.DataBind();
        }

        protected void btnAgregarMaquina_Click(Object sender, EventArgs e) {
            String descrNvaMaq = txtNuevaMaquina.Text;
            DateTime fechaGarantia = new DateTime();
            DateTime fechaCompra = new DateTime();

            if ((!hdfId.Value.Equals("")) && (!hdfId.Value.Equals("&nbsp;")) && (!hdfModificar.Value.Equals("SI"))) {
                //doble click en la grilla -> edita
                CargarInfoModificar();
                btnAgregarMaquina.Text = "guardar cambios";
                btnCambiarEstado.Enabled = true;

            } else { //agrega nueva o guarda cambios 
                if ((hdfModificar.Value.Equals("SI")) && (hdfId2.Value.Equals(hdfId.Value))) {
                    //está modificando por la flauta!

                    if (!txtFechaGarantia.Text.Equals("")) {
                        fechaGarantia = Convert.ToDateTime(txtFechaGarantia.Text);
                    } else {
                        fechaGarantia = System.DateTime.Now;
                    }

                    if (!descrNvaMaq.Equals("")) {
                       
                        try {
                            int id = Convert.ToInt32(hdfId.Value.ToString());
                            var r = maquinasClient.ModificarMaquina(id, descrNvaMaq, fechaGarantia);
                        if (!r.error) {
                                refrescador();
                                hdfModificar.Value = "";
                                hdfId.Value = "";
                                hdfId2.Value = "";
                                lblResultadoAgregar.Text = "Máquina modificada correctamente";

                            }
                        } catch {
                            lblResultadoAgregar.Text = "Error indeterminado";
                        }
                        
                    } else {
                        refrescador();
                        lblResultadoAgregar.Text = "no se pudo guardar, faltan datos";
                    }
                    // fin modificar
                } else if (hdfId2.Value.Equals(hdfId.Value)) { //crear
                    if (!txtFechaCompra.Text.Equals("")) {
                        fechaCompra = Convert.ToDateTime(txtFechaCompra.Text);
                    } else {
                        lblResultadoAgregar.Text = "verfique fecha de compra";
                    }


                    if (!txtFechaGarantia.Text.Equals("")) {
                        fechaGarantia = Convert.ToDateTime(txtFechaGarantia.Text);
                    } else {
                        fechaGarantia = System.DateTime.Now;
                    }

                    if ((!descrNvaMaq.Equals("")) && (!txtFechaCompra.Text.Equals(""))) {
                        var r = maquinasClient.CrearMaquina(descrNvaMaq, fechaCompra, fechaGarantia);
                        if (!r.error) {
                            refrescador();
                            lblResultadoAgregar.Text = "Máquina agregada correctamente";
                        }
                    } else {
                        refrescador();
                        lblResultadoAgregar.Text = "no se pudo guardar, faltan datos";
                    }
                } else {
                    CargarInfoModificar();
                }
            }
        }

        protected void btnCambiarEstado_Click(Object sender, EventArgs e) {
            if ((hdfModificar.Value.Equals("SI")) && (hdfId2.Value.Equals(hdfId.Value))) {
                SRV_Maquinas.MaquinasSRV objMaqTemp = new SRV_Maquinas.MaquinasSRV();
                try {
                    objMaqTemp = maquinasClient.BuscarMaquina(Convert.ToInt32(hdfId.Value.ToString()));
                    if (objMaqTemp != null) {
                       var r =  maquinasClient.CambiaEstadoMaquina(objMaqTemp.idMaquina);
                        if (!r.error) {
                            refrescador();
                            hdfModificar.Value = "";
                            hdfId.Value = "";
                            hdfId2.Value = "";
                            lblResultadoAgregar.Text = "Cambio de estado exitoso";
                        } else {
                            lblResultadoAgregar.Text = "No pude cambiar estado";
                        }

                    } else {

                        lblResultadoAgregar.Text = "Error indeterminado. Intente nuevamente";
                    }

                } catch { }
            }
        }

        protected void CargarInfoModificar() {
            //descrMaquina, fechaCompra, fechaFinGarantia
            SRV_Maquinas.MaquinasSRV objMaqTemp = new SRV_Maquinas.MaquinasSRV();
            try {
                objMaqTemp = maquinasClient.BuscarMaquina(Convert.ToInt32(hdfId.Value.ToString()));
                if (objMaqTemp != null) {
                    txtNuevaMaquina.Text = objMaqTemp.descrMaquina;
                    txtFechaCompra.Text = objMaqTemp.fechaCompra.ToString("yyyy-MM-dd");
                    //txtFechaCompra.Text = Convert.ToDateTime(objMaqTemp.fechaCompra).ToString("dd/MM/yyyy");
                    if (objMaqTemp.fechaFinGarantia != null) {
                        DateTime dfecha = objMaqTemp.fechaFinGarantia ?? DateTime.Now;
                        txtFechaGarantia.Text = dfecha.ToString("yyyy-MM-dd");
                    } else { txtFechaGarantia.Text = ""; }
                    /*
                     formato del iif()
                     variable = (Condicion)?retorno verdadero : retorno farso;
                     */
                    //txtFechaGarantia.Text = (objMaqTemp.fechaFinGarantia!= null)?(DateTime)objMaqTemp.fechaFinGarantia.ToString("yyyy-MM-dd")?"";
                    hdfModificar.Value = "SI";
                    hdfId2.Value = objMaqTemp.idMaquina.ToString();
                    
                } else { 

                    //error
                }

            } catch { }
           // txtNuevaMaquina.Text = maquinasClient.
            //txtFechaCompra
            //txtFechaGarantia
        }
    }
}