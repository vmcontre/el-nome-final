﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CrearOT.aspx.cs" Inherits="WEB.admin.CrearOT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-body">
        <!--#include file="MenuLateral.html"-->
        <main class="overflow-hidden">
            <header class="page header">
                <div class="content">
                    <h1 class="display-4 mb-0">Crear nueva Orden de Trabajo</h1>
                    <p class="lead text-muted">Detalles</p>
                </div>
            </header>
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblMaquina" runat="server" Text="Máquina:" CssClass="form-label"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlMaquina" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblEncargado" runat="server" Text="Encargado:" CssClass="form-label"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlEncargado" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblFalla" runat="server" Text="Falla:" CssClass="form-label"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlFalla" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblRepuesto" runat="server" Text="Repuesto:" CssClass="form-label"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlRepuesto" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-2">
                        <br />
                        <asp:Button runat="server" ID="btnGuardar" Text="Guardar OT" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
                    </div>
                </div>
            </div>
        </main>
    </div>
</asp:Content>
