﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetallesOT.aspx.cs" Inherits="WEB.admin.DetallesOT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div class="main-body">

        <!--#include file="MenuLateral.html"-->

         <main class="overflow-hidden">
            <header class="page header">
                <div class="content">
                    <div class="col-md-6">
                        <h1 class="display-4 mb-0">Listado Detalles</h1>
                        <p class="lead text-muted">Orden de Trabajo</p>
                        <br />
                        <br />
                    </div>                    
                </div>
            </header>
           
                 <div  class="content">

                     <div class="row">
                         <div class="col-md-2">
                             <asp:Label ID="lblCreador" runat="server" Text="Creador:" CssClass="form-label"></asp:Label>
                         </div>
                         <div class="col-md-4">
                             <asp:Label ID="lblCreador2" runat="server" Text="lblCreador2" CssClass="form-label"></asp:Label>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-md-2">
                             <asp:Label ID="lblEncargado" runat="server" Text="Encargado:" CssClass="form-label"></asp:Label>
                         </div>
                         <div class="col-md-4">
                             <asp:Label ID="lblEncargado2" runat="server" Text="lblEncargado2" CssClass="form-label"></asp:Label>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-md-2">
                             <asp:Label ID="lblFechaCreacion" runat="server" Text="Fecha Creación:" CssClass="form-label"></asp:Label>

                         </div>
                         <div class="col-md-4">
                             <asp:Label ID="lblFechaCreacion2" runat="server" Text="lblFechaCreacion2" CssClass="form-label"></asp:Label>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-md-2">
                             <asp:Label ID="lblEstado" runat="server" Text="Estado" CssClass="form-label"></asp:Label>
                         </div>
                         <div class="col-md-4">
                             <asp:Label ID="lblEstado2" runat="server" Text="lblEstado2" CssClass="form-label"></asp:Label>
                         </div>
                     </div>
                 </div>

                 <div  class="content">
                     <div class="row">
                         <asp:Label ID="lblTecnico" runat="server" Text="Técnico:" CssClass="form-label"></asp:Label>
                         <asp:DropDownList runat="server" ID="ddlTecnico" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                         <asp:Label ID="lblFalla" runat="server" Text="Falla:" CssClass="form-label"></asp:Label>
                         <asp:DropDownList runat="server" ID="ddlFalla" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                         <asp:Label ID="lblRepuesto" runat="server" Text="Repuesto:" CssClass="form-label"></asp:Label>
                         <asp:DropDownList runat="server" ID="ddlRepuesto" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                     </div>
                     <div class="row">
                         <div class="col-md-5">
                             <br />
                             <asp:Button ID="btnAgregarDOT" runat="server" Text="Agregar Detalles" CssClass="btn btn-primary" OnClick="btnAgregarDOT_Click" />
                             <asp:Button ID="btnDetalles" runat="server" Text="Detalles" CssClass="btn btn-primary" OnClick="btnDetalles_Click" />
                         </div>
                     </div>
                 </div>
           


                <div class="row">
                    <div class="col-md-3">
                        <br />
                        <br />
                        <asp:DropDownList runat="server" ID="ddlEstadoOT" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <br />
                        <br />
                        <asp:Button ID="btnCerrarOT" runat="server" Text="Cambiar Estado Orden de Trabajo" CssClass="btn btn-primary" OnClick="btnCerrarOT_Click" />
                        <p class="lead text-muted">
                            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
           
            <div class="content"> 
                <asp:HiddenField runat="server" ID="hdfId" />

                <div class="table-responsive">
                    <asp:GridView ID="gvDetalleOT" runat="server" AutoGenerateColumns="false"
                        CssClass="table table-hover table-clean" OnRowCreated="gvDetalleOT_RowCreated" OnRowDataBound="gvDetalleOT_RowDataBound" >
                        <HeaderStyle CssClass="table-clean" />
                        <Columns>
                            <asp:BoundField DataField="idDOT" HeaderText="ID dOT" />
                            <asp:BoundField DataField="idOT" HeaderText="ID OT" />
                            <asp:BoundField DataField="nombreTecnico" HeaderText="Nombre Técnico" />
                            <asp:BoundField DataField="NombreFalla" HeaderText="Falla" />
                            <asp:BoundField DataField="DescRepuesto" HeaderText="Repuesto"   />
                            <asp:BoundField DataField="fecha" HeaderText="Fecha" DataFormatString="{0:d}" />
                            
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-3">
                <asp:Button ID="btnVolver" runat="server" Text="Volver a Listado" CssClass="btn btn-primary" OnClick="btnVolver_Click" />
            </div>
        </main>
    </div>

</asp:Content>
