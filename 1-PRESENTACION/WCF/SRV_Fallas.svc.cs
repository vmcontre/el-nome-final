﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_Fallas" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_Fallas.svc o SRV_Fallas.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_Fallas : ISRV_Fallas {
        FallasNeg objFallasNeg = new FallasNeg();

        public FallasSRV BuscarFalla(Int32 idFalla) {
            fallas objFallas = new fallas();
            FallasSRV objFallasSvr = new FallasSRV();

            objFallas = objFallasNeg.BuscarFalla(idFalla);

            if (objFallas != null) {
                objFallasSvr.idFalla = objFallas.idFalla;
                objFallasSvr.descrFalla = objFallas.descrFalla;
                objFallasSvr.nombreFalla = objFallas.nombreFalla;
                objFallasSvr.sugerenciaFalla = objFallas.sugerenciaFalla;
                return objFallasSvr;
            } else {
                return null;
            }
        }

        public ResultadoDTO CrearFalla(String nombre, string descripcion, string sugerencia) {
            ResultadoDTO r = null;
            fallas objFalla = new fallas();
            objFalla.nombreFalla = nombre;
            objFalla.descrFalla = descripcion;
            objFalla.sugerenciaFalla = sugerencia;
            r = objFallasNeg.CrearFalla(objFalla);
            return r;
        }

        public List<FallasSRV> ListarFallas() {
            List<FallasSRV> listaFallasSvr = new List<FallasSRV>();
            List<ItemFallas> listaItemFallas = new List<ItemFallas>();

            listaItemFallas = objFallasNeg.ListarFallas();

            foreach (ItemFallas i in listaItemFallas) {
                FallasSRV objFallasSvrRet = new FallasSRV() {
                    descrFalla = i.descrFalla,
                    idFalla = i.idFalla,
                    nombreFalla = i.nombreFalla,
                    sugerenciaFalla = i.sugerenciaFalla
                };
                listaFallasSvr.Add(objFallasSvrRet);
            }
            return listaFallasSvr;
        }

        public ResultadoDTO ModificarFallas(Int32 idFalla, String descripcion, String sugerencia) {
            return objFallasNeg.ModificarFallas(idFalla, descripcion, sugerencia);
        }
    }
}
