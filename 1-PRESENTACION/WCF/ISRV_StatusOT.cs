﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_StatusOT" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_StatusOT {
        [OperationContract]
        List<StatusOTSRV> listarStatusOT();
        [OperationContract]
        String BuscarStatusOT(int id);
        [OperationContract]
        int BuscarIDStatusOT(String descrStatus);
    }

    public class StatusOTSRV {
        [DataMember]
        public int idStatusOT { get; set; }
        [DataMember]
        public string DescrStatus { get; set; }
    }
}
