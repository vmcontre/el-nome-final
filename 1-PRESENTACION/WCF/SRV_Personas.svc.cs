﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_Personas" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_Personas.svc o SRV_Personas.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_Personas : ISRV_Personas {
        PersonasNeg objPersonaNeg = new PersonasNeg();

        public PersonasSRV BuscarID(Int32 idUsuario) {
            ItemPersonas objPersonaTemmp = new ItemPersonas();
            PersonasSRV objPersonaSRV = new PersonasSRV();

            objPersonaTemmp = objPersonaNeg.BuscarID(idUsuario);
            if (objPersonaTemmp != null) {
                objPersonaSRV.activo = objPersonaTemmp.activo;
                objPersonaSRV.idPersona = objPersonaTemmp.idPersona;
                objPersonaSRV.idCargo = objPersonaTemmp.idCargo;
                objPersonaSRV.nombreCargo = objPersonaTemmp.nombreCargo;
                objPersonaSRV.nombrePersona = objPersonaTemmp.nombrePersona;
                objPersonaSRV.userName = objPersonaTemmp.username;
                return objPersonaSRV;
            } else {
                return null;
            }
        }

        public ResultadoDTO CambiaEstadoPersona(Int32 idPersona) {
            return objPersonaNeg.CambiaEstadoPersona(idPersona);
        }

        public ResultadoDTO CambiarContraseña(Int32 idPersona, String nuevaPasswd) {
            return objPersonaNeg.CambiarContraseña(idPersona, nuevaPasswd);
        }

        public ResultadoDTO CrearPersona(String nombrePersona, Int32 idCargo, String userName, String pswd) {
            personas objNvaPersona = new personas();
            objNvaPersona.nombrePersona = nombrePersona;
            objNvaPersona.idCargo = idCargo;
            objNvaPersona.userName = userName;
            objNvaPersona.passwd = pswd;
            objNvaPersona.activo = true;

            return objPersonaNeg.CrearPersona(objNvaPersona);
        }

        public Boolean ExisteUserName(String username) {
            return objPersonaNeg.ExisteUserName(username);
        }

        public List<PersonasSRV> ListarPersonas() {
            List<PersonasSRV> listaPersonasSRV = new List<PersonasSRV>();
            List<ItemPersonas> listaItemPersonas = new List<ItemPersonas>();

            listaItemPersonas = objPersonaNeg.ListarPersonas();

            foreach (ItemPersonas p in listaItemPersonas) {
                PersonasSRV objTemp = new PersonasSRV() {
                    activo = p.activo,
                    idCargo = p.idCargo,
                    idPersona = p.idPersona,
                    nombreCargo = p.nombreCargo,
                    nombrePersona = p.nombrePersona,
                    userName = p.username
                };
                listaPersonasSRV.Add(objTemp);
            }
            return listaPersonasSRV;
        }

        public List<PersonasSRV> ListarPersonasActivas() {
            List<PersonasSRV> listaPersonasSRV = new List<PersonasSRV>();
            List<ItemPersonas> listaItemPersonas = new List<ItemPersonas>();

            listaItemPersonas = objPersonaNeg.ListarPersonas();

            foreach (ItemPersonas p in listaItemPersonas) {
                if (p.activo) {
                    PersonasSRV objTemp = new PersonasSRV() {
                        activo = p.activo,
                        idCargo = p.idCargo,
                        idPersona = p.idPersona,
                        nombreCargo = p.nombreCargo,
                        nombrePersona = p.nombrePersona,
                        userName = p.username
                    };
                    listaPersonasSRV.Add(objTemp);
                }
            }
            return listaPersonasSRV;
        }

        public ResultadoDTO ModificarPersona(Int32 idPersona, String nombrePersona, String UserName, Int32 idCargo) {
            return objPersonaNeg.ModificarPersona(idPersona, nombrePersona, UserName, idCargo);
        }

        public Boolean ValidaLogin(String username) {
            return objPersonaNeg.ValidaLogin(username);
        }
    }
}
