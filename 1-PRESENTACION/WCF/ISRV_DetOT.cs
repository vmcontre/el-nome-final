﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_DetOT" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_DetOT {
        [OperationContract]
        ResultadoDTO CrearDetalleOT(int idOT, int idTec, int idRep, int idFalla);
        [OperationContract]
        List<DetalleOTSRV> ListarDOT(int id);
        [OperationContract]
        DetalleOTSRV BuscarDetalleOT(int id);
        [OperationContract]
        ResultadoDTO ModificarDetOT(int id, int idTec, int idRep, int idFalla);
    }

    public class DetalleOTSRV {
        [DataMember]
        public int idOT { get; set; }
        [DataMember]
        public int idDOT { get; set; }
        [DataMember]
        public String nombreTecnico { get; set; }
        [DataMember]
        public String nombreFalla { get; set; }
        [DataMember]
        public DateTime fecha { get; set; }
        [DataMember]
        public Int32 idTecnico { get; set; }
        [DataMember]
        public Int32 idRepuesto { get; set; }
        [DataMember]
        public String DescRepuesto { get; set; }
        [DataMember]
        public Int32 idFalla { get; set; }
    }
}
