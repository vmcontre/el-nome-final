﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_Repuestos" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_Repuestos {
        [OperationContract]
        ResultadoDTO CrearRepuesto(String descRep, int stockRep);
        [OperationContract]
        List<RepuestosSRV> ListarRepuestos();
        [OperationContract]
        RepuestosSRV BuscarRepuesto(int id);
        [OperationContract]
        ResultadoDTO ActualizarStockRepuesto(int idRepuesto, int stockRepuesto, String nuevoNombreRep);
        [OperationContract]
        List<RepuestosSRV> ListarRepuestosConStock();
        [OperationContract]
        ResultadoDTO BorrarRepuesto(int idRep);     
    }
    public class RepuestosSRV {
        [DataMember]
        public int idRepuesto { get; set; }
        [DataMember]
        public string descrRepuesto { get; set; }
        [DataMember]
        public int stockRepuesto { get; set; }
    }

}
