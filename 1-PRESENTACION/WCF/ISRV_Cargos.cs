﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_Cargos" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_Cargos {
        [OperationContract]
        ResultadoDTO CrearCargo(String descrCargo);
        [OperationContract]
        List<CargosSRV> ListarCargos();
        [OperationContract]
        ResultadoDTO ModificarCargos(int idCargo, String descrCargo);
        [OperationContract]
        CargosSRV BuscarCargos(int idCargo);
        [OperationContract]
        ResultadoDTO EliminarCargo(int idCargo);
    }

    public class CargosSRV {
        [DataMember]
        public Int32 idCargo { get; set; }
        [DataMember]
        public string DescripcionCargo { get; set; }
    }
}
