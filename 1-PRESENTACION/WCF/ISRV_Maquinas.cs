﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_Maquinas" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_Maquinas {
        [OperationContract]
        ResultadoDTO CrearMaquina(String descrMaquina, DateTime fechaCompra, DateTime? fechaGarantia);
        [OperationContract]
        List<MaquinasSRV> ListarMaquinas();
        [OperationContract]
        ResultadoDTO ModificarMaquina(int idMaq, String nvaDescr, DateTime? nuevaFechaGtia);
        [OperationContract]
        ResultadoDTO CambiaEstadoMaquina(int idMaq);
        [OperationContract]
        List<MaquinasSRV> ListarMaquinasActivas();
        [OperationContract]
        MaquinasSRV BuscarMaquina(int idMaq);
    }
    public class MaquinasSRV {
        [DataMember]
        public int idMaquina { get; set; }
        [DataMember]
        public string descrMaquina { get; set; }
        [DataMember]
        public DateTime fechaCompra { get; set; }
        [DataMember]
        public DateTime? fechaFinGarantia { get; set; }
        [DataMember]
        public bool activo { get; set; }
    }

}
