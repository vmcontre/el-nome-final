﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_DetOT" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_DetOT.svc o SRV_DetOT.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_DetOT : ISRV_DetOT {
        DetalleOTNeg detalleOTNeg = new DetalleOTNeg();
        public DetalleOTSRV BuscarDetalleOT(Int32 id) {
            DetalleOTDTO objDetalleOT = new DetalleOTDTO();
            DetalleOTSRV objDetalleOTSvr = new DetalleOTSRV();
            objDetalleOT = detalleOTNeg.BuscarDetalleOT_DTO(id);
            if (objDetalleOT != null) {
                objDetalleOTSvr.fecha = objDetalleOT.fecha;
                objDetalleOTSvr.idDOT = objDetalleOT.idDOT;
                objDetalleOTSvr.idOT = objDetalleOT.idOT;
                objDetalleOTSvr.nombreTecnico = objDetalleOT.nombreTecnico;
                objDetalleOTSvr.nombreFalla = objDetalleOT.nombreFalla;
                objDetalleOTSvr.DescRepuesto = objDetalleOT.nombreRepuesto;
                return objDetalleOTSvr;
            } else {
                return null;
            }
        }

        public ResultadoDTO CrearDetalleOT(int idOT, int idTec, int idRep, int idFalla) {
            ResultadoDTO r = new ResultadoDTO();
            detalleOT objDet = new detalleOT();
            objDet.idOT = idOT;
            objDet.idTecnico = idTec;
            objDet.idRepuesto = idRep;
            objDet.idFalla  = idFalla;
            objDet.fecha = DateTime.Now;
            r = detalleOTNeg.CrearDetalleOT(objDet);
            return r;
        }

        public List<DetalleOTSRV> ListarDOT(Int32 id) {
            List<DetalleOTSRV> ListDetallesSvr = new List<DetalleOTSRV>();
            List<DetalleOTDTO> objListaSvr = new List<DetalleOTDTO>();

            objListaSvr = detalleOTNeg.ListarDOT_DTO(id);

            foreach (DetalleOTDTO c in objListaSvr) {
                DetalleOTSRV objDetallesSvrRet = new DetalleOTSRV() {
                    nombreTecnico = c.nombreTecnico,
                    nombreFalla = c.nombreFalla,
                    idDOT = c.idDOT,
                    idOT = c.idOT,
                    fecha = c.fecha,
                    DescRepuesto = c.nombreRepuesto
                };
                ListDetallesSvr.Add(objDetallesSvrRet);
            }
            return ListDetallesSvr;
        }

        public ResultadoDTO ModificarDetOT(Int32 id, Int32 idTec, Int32 idRep, Int32 idFalla) {
            return detalleOTNeg.ModificarDetOT(id, idTec, idRep, idFalla);
        }
    }
}
