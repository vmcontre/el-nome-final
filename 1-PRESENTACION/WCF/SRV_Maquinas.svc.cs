﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_Maquinas" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_Maquinas.svc o SRV_Maquinas.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_Maquinas : ISRV_Maquinas {
        MaquinasNeg objMaquinasNeg = new MaquinasNeg();

        public MaquinasSRV BuscarMaquina(Int32 idMaq) {
            MaquinasSRV objMaquinaEncontrada = new MaquinasSRV();
            ItemMaquinas objMaqTemp =  objMaquinasNeg.BuscarMaquina(idMaq);
            if (objMaqTemp != null) {
                objMaquinaEncontrada.activo = objMaqTemp.activo;
                objMaquinaEncontrada.descrMaquina = objMaqTemp.descrMaquina;
                objMaquinaEncontrada.fechaCompra = objMaqTemp.fechaCompra;
                objMaquinaEncontrada.fechaFinGarantia = objMaqTemp.fechaFinGtia;
                objMaquinaEncontrada.idMaquina = objMaqTemp.idMaquina;
                return objMaquinaEncontrada;
            } 
            else {
                return null;
            }
        }

        public ResultadoDTO CambiaEstadoMaquina(Int32 idMaq) {
            return objMaquinasNeg.CambiaEstadoMaquina(idMaq);
        }

        public ResultadoDTO CrearMaquina(String descrMaquina, DateTime fechaCompra, DateTime? fechaGarantia) {
            ResultadoDTO r = null;
            maquinas objMaquina = new maquinas();
            objMaquina.activo = true;
            objMaquina.descrMaquina = descrMaquina;
            objMaquina.fechaCompra = fechaCompra;
            if (fechaGarantia.HasValue) {
                objMaquina.fechaFinGarantia = fechaGarantia;
            }
            r = objMaquinasNeg.CrearMaquina(objMaquina);
            return r;
        }

        public List<MaquinasSRV> ListarMaquinas() {
            List<MaquinasSRV> listaMaquinasSVR = new List<MaquinasSRV>();
            List<ItemMaquinas> objListaSVR = new List<ItemMaquinas>();
            objListaSVR = objMaquinasNeg.ListarMaquinas();

            foreach (ItemMaquinas m in objListaSVR) {
                MaquinasSRV objMaquinasSrvRet = new MaquinasSRV() {
                    activo = m.activo,
                    descrMaquina = m.descrMaquina,
                    fechaCompra = m.fechaCompra,
                    fechaFinGarantia = m.fechaFinGtia,
                    idMaquina = m.idMaquina
                };
                listaMaquinasSVR.Add(objMaquinasSrvRet);
            }
            return listaMaquinasSVR;

        }

        public List<MaquinasSRV> ListarMaquinasActivas() {
            List<MaquinasSRV> listaMaquinasSVR = new List<MaquinasSRV>();
            List<ItemMaquinas> objListaSVR = new List<ItemMaquinas>();
            objListaSVR = objMaquinasNeg.ListarMaquinas();

            foreach (ItemMaquinas m in objListaSVR) {
                if (m.activo) {
                    MaquinasSRV objMaquinasSrvRet = new MaquinasSRV(){
                        activo = m.activo,
                        descrMaquina = m.descrMaquina,
                        fechaCompra = m.fechaCompra,
                        fechaFinGarantia = m.fechaFinGtia,
                        idMaquina = m.idMaquina
                    };
                    listaMaquinasSVR.Add(objMaquinasSrvRet);
                }
            }
            return listaMaquinasSVR;

        }

        public ResultadoDTO ModificarMaquina(Int32 idMaq, String nvaDescr, DateTime? nuevaFechaGtia) {
            return objMaquinasNeg.ModificarMaquina(idMaq, nvaDescr, nuevaFechaGtia);
        }
    }
}
