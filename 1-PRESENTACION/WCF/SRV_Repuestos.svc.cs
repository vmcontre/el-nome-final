﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_Repuestos" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_Repuestos.svc o SRV_Repuestos.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_Repuestos : ISRV_Repuestos {
        RepuestosNeg objRepuestosNeg = new RepuestosNeg();
        public ResultadoDTO ActualizarStockRepuesto(Int32 idRepuesto, Int32 stockRepuesto, String nuevoNombreRep) {
            return objRepuestosNeg.ActualizarStockRepuesto(idRepuesto, stockRepuesto, nuevoNombreRep);
        }

        public ResultadoDTO BorrarRepuesto(Int32 idRep) {
            repuestos objTemp = objRepuestosNeg.BuscarRepuesto(idRep);

            if (objTemp != null) {
                return objRepuestosNeg.BorrarRepuesto(idRep);
            } else {
                return null;
            }
        }

        public RepuestosSRV BuscarRepuesto(Int32 id) {
            RepuestosSRV objRepuestoSVR = new RepuestosSRV();
            repuestos objTemp = new repuestos();
            objTemp = objRepuestosNeg.BuscarRepuesto(id);

            if (objTemp != null) {
                objRepuestoSVR.idRepuesto = objTemp.idRepuesto;
                objRepuestoSVR.descrRepuesto = objTemp.descrRepuesto;
                objRepuestoSVR.stockRepuesto = objTemp.stockRepuesto;
                return objRepuestoSVR;
            } else {
                return null;
            }
        }

        public ResultadoDTO CrearRepuesto(String descRep, Int32 stockRep) {
            repuestos objNvoRepuesto = new repuestos();
            objNvoRepuesto.descrRepuesto = descRep;
            objNvoRepuesto.stockRepuesto = stockRep;

            return objRepuestosNeg.CrearRepuesto(objNvoRepuesto);
        }

        public List<RepuestosSRV> ListarRepuestos() {
            List<RepuestosSRV> listaRepuestosSVR = new List<RepuestosSRV>();
            List<ItemRepuestos> listaItemRepuestos = new List<ItemRepuestos>();

            listaItemRepuestos = objRepuestosNeg.ListarRepuestos();

            foreach (ItemRepuestos i in listaItemRepuestos) {
                RepuestosSRV objTemp = new RepuestosSRV() {
                    idRepuesto = i.idRepuesto,
                    descrRepuesto = i.descrRepuesto,
                    stockRepuesto = i.stockRepuesto
                };
                listaRepuestosSVR.Add(objTemp);
            }
            return listaRepuestosSVR;
        }

        public List<RepuestosSRV> ListarRepuestosConStock() {
            List<RepuestosSRV> listaRepuestosSVR = new List<RepuestosSRV>();
            List<ItemRepuestos> listaItemRepuestos = objRepuestosNeg.ListarRepuestos();

            foreach (ItemRepuestos i in listaItemRepuestos) {
                if (i.stockRepuesto > 0) {
                    RepuestosSRV objTemp = new RepuestosSRV() {
                        idRepuesto = i.idRepuesto,
                        descrRepuesto = i.descrRepuesto,
                        stockRepuesto = i.stockRepuesto
                    };
                    listaRepuestosSVR.Add(objTemp);
                }
            }
            return listaRepuestosSVR;
        }

        
    }
}
