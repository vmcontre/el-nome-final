﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_Personas" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_Personas {
        [OperationContract]
        ResultadoDTO CrearPersona(String nombrePersona, int idCargo, String userName, String pswd);
        [OperationContract]
        ResultadoDTO CambiaEstadoPersona(int idPersona);
        [OperationContract]
        ResultadoDTO ModificarPersona(int idPersona, String nombrePersona, String UserName, int idCargo);
        [OperationContract]
        List<PersonasSRV> ListarPersonas();
        [OperationContract]
        List<PersonasSRV> ListarPersonasActivas();
        [OperationContract]
        Boolean ValidaLogin(String username);
        [OperationContract]
        PersonasSRV BuscarID(int idUsuario);
        [OperationContract]
        ResultadoDTO CambiarContraseña(int idPersona, String nuevaPasswd);
        [OperationContract]
        Boolean ExisteUserName(String username);
    }

    public class PersonasSRV {
        [DataMember]
        public int idPersona { get; set; }
        [DataMember]
        public string nombrePersona { get; set; }
        [DataMember]
        public int idCargo { get; set; }
        [DataMember]
        public string nombreCargo { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public string userName { get; set; }
        [DataMember]
        public string passwd { get; set; }
    }
}
