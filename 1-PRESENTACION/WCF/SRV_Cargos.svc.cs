﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_Cargos" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_Cargos.svc o SRV_Cargos.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_Cargos : ISRV_Cargos {
        CargosNeg objCargosNeg = new CargosNeg();

        public CargosSRV BuscarCargos(Int32 idCargo) {
            cargos objCargo = new cargos();
            CargosSRV objCargoSVR = new CargosSRV();
            objCargo = objCargosNeg.BuscarCargos(idCargo);
            if (objCargo != null) {
                objCargoSVR.idCargo = objCargo.idCargo;
                //objCargoSVR.NombreCargo = objCargo.;
                objCargoSVR.DescripcionCargo = objCargo.descrCargo;
               
                return objCargoSVR;
            } else {
                return null;
            }
        }

        public ResultadoDTO CrearCargo(String descrCargo) {
            ResultadoDTO r = null;
            cargos objCargo = new cargos();
            objCargo.descrCargo = descrCargo;
            r = objCargosNeg.CrearCargo(objCargo);
            return r;
        }

        public ResultadoDTO EliminarCargo(Int32 idCargo) {
            ResultadoDTO r = null;
            r = objCargosNeg.EliminarCargo(idCargo);
            return r;
        }

        public List<CargosSRV> ListarCargos() {
            List<CargosSRV> listCargosSvr = new List<CargosSRV>();
            List<ItemCargos> objListaSvr = new List<ItemCargos>();

            objListaSvr = objCargosNeg.ListarCargos();

            foreach (ItemCargos c in objListaSvr) {
                CargosSRV objCargosSvrRet = new CargosSRV() {
                    idCargo = c.idCargo,
                    DescripcionCargo = c.descrCargo
                };

                listCargosSvr.Add(objCargosSvrRet);
            }
            
            return listCargosSvr;
        }

        public ResultadoDTO ModificarCargos(Int32 idCargo, String descrCargo) {
            return objCargosNeg.ModificarCargos(idCargo, descrCargo);
        }
    }
}
