﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WEB.admin.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-body">

        <!--#include file="MenuLateral.html"-->

        <main class="overflow-hidden">
            <header class="page header">
                <div class="content">
                    <div class="col-md-6">
                        <h1 class="display-4 mb-0">Historico OT</h1>
                        <p class="lead text-muted">Detalles de OT</p>
                     </div>
                    <div class="row">
                        <div class="col-md-3">
                            <asp:Button ID="btnAgregarOT" runat="server" Text="Crear Nueva Orden de Trabajo" CssClass="btn btn-primary" OnClick="btnAgregarOT_Click" />
                        </div>
                        <div class="col-md-3">
                            <asp:Button ID="btnDetalles" runat="server" Text="Ver Detalles" CssClass="btn btn-primary" OnClick="btnDetalles_Click" />
                        </div>
                    </div>
                </div>
            </header>
            <div class="content"> 


                <div class="table-responsive">
                    <asp:GridView ID="gvHistoricoOT" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-clean" OnRowCreated="gvHistoricoOT_RowCreated" OnRowDataBound="gvHistoricoOT_RowDataBound" >
                        <HeaderStyle CssClass="table-clean" />
                        <Columns>
                            <asp:BoundField DataField="idOT" HeaderText="ID OT" />
                            <asp:BoundField DataField="nombreMaquina" HeaderText="Nombre Máquina" />
                            <asp:BoundField DataField="nombreCreador" HeaderText="Creador OT" />
                            <asp:BoundField DataField="nombreEncargado" HeaderText="Tecnico Encargado" />
                            <asp:BoundField DataField="estadoOT" HeaderText="Estado OT" />
                            <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha Creación"  DataFormatString="{0:d}" />
                            <asp:BoundField DataField="fechaCierre" HeaderText="Fecha Cierre"  DataFormatString="{0:d}" />
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField runat="server" ID="hdfId" />
                    <asp:HiddenField runat="server" ID="hdfEstado" />
                </div>
            </div>
        </main>
    </div>
</asp:Content>
