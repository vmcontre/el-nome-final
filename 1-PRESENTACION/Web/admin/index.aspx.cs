﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.admin {
    public partial class index : System.Web.UI.Page {
        SRV_OrdenTrabajo.SRV_OrdenTrabajoClient ordenTrabajoClient = new SRV_OrdenTrabajo.SRV_OrdenTrabajoClient();
        protected void Page_Load(object sender, EventArgs e) {
            refrescador();
            //btnDetalles.Visible = false;
        }

        protected void gvHistoricoOT_RowCreated(Object sender, GridViewRowEventArgs e) {
            e.Row.Cells[0].Visible = true;
        }

        protected void gvHistoricoOT_RowDataBound(Object sender, GridViewRowEventArgs e) {
            if (e.Row.RowType != DataControlRowType.Header) {
                //e.Row.Attributes.Add("onclick", "$('#" + hdfEstado.ClientID + "').val('" + e.Row.Cells[4].Text + "'); $('#" + btnDetalles.ClientID + "').click();");
                e.Row.Attributes.Add("ondblclick", "$('#" + hdfId.ClientID + "').val('" + e.Row.Cells[0].Text + "'); $('#" + btnDetalles.ClientID + "').click();");
            }
        }
        protected void refrescador() {
            //grilla
            gvHistoricoOT.DataSource = ordenTrabajoClient.ListarOrdenTrabajo();
            gvHistoricoOT.DataBind();
        }

        protected void btnAgregarOT_Click(Object sender, EventArgs e) {
            Response.Redirect("~/admin/CrearOT.aspx");
        }

        protected void btnDetalles_Click(Object sender, EventArgs e) {
            if ((!hdfId.Value.Equals("")) && (!hdfId.Value.Equals("&nbsp;"))){ 
                Session["SesionDetalles"] = hdfId.Value;
                Response.Redirect("~/admin/DetallesOT.aspx");
            }
        }
    }
}