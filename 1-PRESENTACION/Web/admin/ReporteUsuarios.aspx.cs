﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.admin {
    public partial class ReporteUsuarios : System.Web.UI.Page {
        SRV_Personas.SRV_PersonasClient personasClient = new SRV_Personas.SRV_PersonasClient();
        SRV_Cargos.SRV_CargosClient cargosClient = new SRV_Cargos.SRV_CargosClient();
        SRV_Login.UsuarioSRV objUsuarioConectado = new SRV_Login.UsuarioSRV();

        SRV_Personas.PersonasSRV objPersonaTemp = new SRV_Personas.PersonasSRV();
        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                refrescador();
                
            }
        }

        protected void refrescador()
        {
            //textboxes
            txtNuevaContrasena.Text = "";
            txtNuevaContrasena2.Text = "";
            txtNuevoUserName.Text = "";
            txtNuevoUsuario.Text = "";
            lblContrasenasIguales.Text = "";
            lblTitulo.Text = "Guardar nuevo Usuario";
            btnAgregarUsuario.Text = "Agregar";
            lblUsuarioExistente.Text = "";

            //grilla
            gvUsuarios.DataSource = personasClient.ListarPersonas();
            gvUsuarios.DataBind();

            ddlCargo.DataSource = cargosClient.ListarCargos();
            ddlCargo.DataTextField = "DescripcionCargo";
            ddlCargo.DataValueField = "idCargo";
            ddlCargo.DataBind();
            ddlCargo.Items.Insert(0, new ListItem(""));

            //boton cambiar estado
            btnCambiarEstado.Enabled = false;
            
            //confirma superadmin para crear nuevos usuarios
            objUsuarioConectado = (SRV_Login.UsuarioSRV)Session["sesionUsuario"];
            if (objUsuarioConectado.CargoPersona.Equals("Super Administrador")) {
                ddlCargo.Enabled = true;
                txtNuevaContrasena.Enabled = true;
                txtNuevaContrasena2.Enabled = true;
                txtNuevoUserName.Enabled = true;
                txtNuevoUsuario.Enabled = true;
                btnAgregarUsuario.Enabled = true;
            } else {
                ddlCargo.Enabled = false;
                txtNuevaContrasena.Enabled = false;
                txtNuevaContrasena2.Enabled = false;
                txtNuevoUserName.Enabled = false;
                txtNuevoUsuario.Enabled = false;
                btnAgregarUsuario.Enabled = false;
            }
        }

        protected Boolean validador() {
            
            if (!txtNuevaContrasena.Text.Trim().Equals(txtNuevaContrasena2.Text.Trim())) {
                lblContrasenasIguales.Text = "Contraseñas no coinciden";
                return false;
            }
            if (txtNuevaContrasena.Text.Length > 10)  {
                lblContrasenasIguales.Text = "largo máximo de contraseña 10 caracteres";
                return false;
            }
            if (txtNuevoUserName.Text.Length > 10) {
                lblUsuarioExistente.Text = "largo máximo de nombre de usuario 10 caracteres";
                return false;
            }
            if (txtNuevoUserName.Text.Contains(" ")) {
                lblUsuarioExistente.Text = "nombre de usuario contiene espacios. Corrija";
                return false;
            }

            if (ddlCargo.Text.Equals("")) return false;

            if (!hdfModificar.Value.Equals("SI")) { //solo verifico si hay cotraseña al guardar nuevo usuario. Al modificar, si está en blanco, no  la cambio.
                if (txtNuevaContrasena.Text.Trim().Equals("")) return false;
            }
            if (txtNuevoUserName.Text.Trim().Equals("")) return false;
            //valido nuevo username
            if ((!hdfModificar.Value.Equals("SI")) && (personasClient.ExisteUserName(txtNuevoUserName.Text.Trim()))) {
                lblUsuarioExistente.Text = "nombre de usuario ya existe";
                return false;
            }
            //valido cambio de username
            if ((hdfModificar.Value.Equals("SI")) && (!hdfUserNameActual.Value.Equals(txtNuevoUserName.Text.Trim())) ){
                if (personasClient.ExisteUserName(txtNuevoUserName.Text.Trim())) {
                    lblUsuarioExistente.Text = "nombre de usuario ya existe";
                    return false;
                }
            }            
            
            if (txtNuevoUsuario.Text.Trim().Equals("")) {
                return false;
            } else {
                return true;
            }
        }

        protected void btnAgregarUsuario_Click(Object sender, EventArgs e) {
            if ((!hdfId.Value.Equals("")) && (!hdfId.Value.Equals("&nbsp;")) && (!hdfModificar.Value.Equals("SI"))) {
                //doble click en la grilla -> edita
                lblTitulo.Text = "Editar Usuario Existente";
                btnAgregarUsuario.Text = "guardar cambios";
                CargarInfoModificar();
                btnCambiarEstado.Enabled = true;


            } else { //apretó agregar directamente
                if (validador()) {
                    if ((hdfModificar.Value.Equals("SI")) && (hdfId2.Value.Equals(hdfId.Value))) { //modificar

                        objPersonaTemp = personasClient.BuscarID(Convert.ToInt32(hdfId.Value));

                        if (!txtNuevaContrasena.Text.Equals("")) {
                            //cambio contraseña
                            var rr = personasClient.CambiarContraseña(objPersonaTemp.idPersona, txtNuevaContrasena.Text.Trim());
                            if (rr.error) {
                                lblResultadoAgregar.Text = "no pude cambiar la contraseña";
                            }
                        }

                        var r = personasClient.ModificarPersona(objPersonaTemp.idPersona, txtNuevoUsuario.Text.Trim(), txtNuevoUserName.Text.Trim(), Int32.Parse(ddlCargo.SelectedValue));
                        if (!r.error) {
                            refrescador();
                            hdfModificar.Value = "";
                            hdfId.Value = "";
                            hdfId2.Value = "";
                            hdfUserNameActual.Value = "";
                            lblResultadoAgregar.Text = "Usuario modificado exitosamente";
                        } else {
                            lblResultadoAgregar.Text = r.mensaje;
                        }

                    } else if (hdfId2.Value.Equals(hdfId.Value)) { //crear
                        var r = personasClient.CrearPersona(txtNuevoUsuario.Text.Trim(), Int32.Parse(ddlCargo.SelectedValue), txtNuevoUserName.Text.Trim(), txtNuevaContrasena.Text.Trim());
                        if (!r.error) {
                            refrescador();
                            lblResultadoAgregar.Text = "Usuario agregado exitosamente";
                        } else {
                            lblResultadoAgregar.Text = r.mensaje;
                        }
                    } else {
                        CargarInfoModificar();
                    }
                    
                } else {
                    lblResultadoAgregar.Text = "Verifique la información";
                }
            }
             
        }

        protected void gvUsuarios_RowDataBound(Object sender, GridViewRowEventArgs e) {
            if (e.Row.RowType != DataControlRowType.Header) {
                e.Row.Attributes.Add("onclick", "$('#" + hdfId.ClientID + "').val('" + e.Row.Cells[0].Text + "'); $('#" + btnAgregarUsuario.ClientID + "').click();");
                hdfId.Value = e.Row.Cells[0].Text;
            }
        }
        
        protected void gvUsuarios_RowCreated(Object sender, GridViewRowEventArgs e) {
            e.Row.Cells[0].Visible = false;
        }

        protected void CargarInfoModificar() {
            objPersonaTemp = personasClient.BuscarID(Convert.ToInt32(hdfId.Value));

            if (objPersonaTemp != null) {
                //cargo info en los campos correspondientes
                ddlCargo.SelectedValue = objPersonaTemp.idCargo.ToString();
                txtNuevaContrasena.Text = "";  
                txtNuevaContrasena2.Text = "";
                txtNuevoUserName.Text = objPersonaTemp.userName;
                txtNuevoUsuario.Text = objPersonaTemp.nombrePersona;
                hdfUserNameActual.Value = objPersonaTemp.userName;
                //hdfId.Value = "";
                hdfModificar.Value = "SI";
                hdfId2.Value = objPersonaTemp.idPersona.ToString();

            }
            
        }

        protected void btnCambiarEstado_Click(Object sender, EventArgs e) {
            if ((hdfModificar.Value.Equals("SI")) && (hdfId2.Value.Equals(hdfId.Value))) {
                objPersonaTemp = personasClient.BuscarID(Convert.ToInt32(hdfId.Value));
                if (objPersonaTemp != null) {
                    var r= personasClient.CambiaEstadoPersona(objPersonaTemp.idPersona);
                    if (!r.error) {
                        refrescador();
                        hdfModificar.Value = "";
                        hdfId.Value = "";
                        hdfId2.Value = "";
                        hdfUserNameActual.Value = "";
                        lblResultadoAgregar.Text = "cambio de estado exitoso";
                    } else {
                        lblResultadoAgregar.Text = "No pude cambiar el estado";
                    }
                }
            }
        }
    }
}