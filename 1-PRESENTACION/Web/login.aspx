﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WEB.login" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

      <title>El Nome - Software Gestión OT</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />

    <!-- Place favicon.ico in the root directory -->
    <link rel="ElNome" href="ElNome.png" />
    <link rel="icon" href="favicon.ico" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,700,900" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet" />

    <!-- themeforest:css -->
    <!--#include file="base.html"-->

</head>
    <body>
	<main>
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-md-6 col-lg-7 fullscreen-md d-flex justify-content-center align-items-center overlay gradient gradient-53 alpha-7 image-background cover" style="background-image:url(https://picsum.photos/1920/1200/?random&gravity=south)">
					<div class="content">
						<h2 class="display-4 display-md-3 color-1 mt-4 mt-md-0">Bienvenido a
							<span class="bold d-block">El Nome</span>
						</h2>
						<p class="lead color-1 alpha-5">Ingresa a tu cuenta</p>
						<hr class="mt-md-6 w-25"/>
					</div>
				</div>
				<div class="col-md-5 col-lg-4 mx-auto">
					<div class="login-form mt-5 mt-md-0">
						<a href="index.aspx"><img src="img/logo.png" class="logo img-responsive mb-4 mb-md-6" alt=""/></a>
						<h1 class="color-5 bold">Iniciar Sesión</h1>
						<p class="color-2 mt-0 mb-4 mb-md-6">¿Aún no tienes una cuenta?
							<a href="register.aspx" class="accent bold">Créalo aquí</a>
						</p>
						<form id="form1" runat="server" class="cozy">
							<asp:Label runat="server"  class="control-label bold small text-uppercase color-2">Nombre Usuario</asp:Label>
							<div class="form-group has-icon">
								<asp:TextBox runat="server"  id="login_username" name="Login[username]" class="form-control form-control-rounded" placeholder="Tu Usuario" required="required"/> 
								<i class="icon fas fa-user"></i>
							</div>
							<asp:Label runat="server" class="control-label bold small text-uppercase color-2">Contraseña</asp:Label>
							<div class="form-group has-icon">
								<asp:TextBox runat="server" TextMode="Password" id="login_password" name="Login[password]" class="form-control form-control-rounded" placeholder="Tu Contraseña" required="required"/>
								<i class="icon fas fa-lock"></i>
							</div>
                            <div>
                                <asp:Label ID="lblResultadoLogin" runat="server" Text=""></asp:Label>
                            </div>
							<div class="form-group d-flex align-items-center justify-content-between">
								<a href="forgot.aspx" class="text-warning small">¿Olvidaste tu contraseña?</a>
								<div class="ajax-button">
									<div class="fas fa-check btn-status text-success success"></div>
									<div class="fas fa-times btn-status text-danger failed"></div>

									<asp:Button runat="server" ID="btnLogin"  type="submit" class="btn btn-accent btn-rounded" Text="Iniciar Sesión" OnClick="btnLogin_Click" />
										<i class="fas fa-long-arrow-alt-right ml-2"></i>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>

	<!-- themeforest:js -->
	<script src="js/01.cookie-consent-util.js"></script>
	<script src="js/02.1.cookie-consent-themes.js"></script>
	<script src="js/02.2.cookie-consent-custom-css.js"></script>
	<script src="js/02.3.cookie-consent-informational.js"></script>
	<script src="js/02.4.cookie-consent-opt-out.js"></script>
	<script src="js/02.5.cookie-consent-opt-in.js"></script>
	<script src="js/02.6.cookie-consent-location.js"></script>

	<script src="js/jquery.js"></script>
	<script src="js/jquery.easing.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/jquery.smartWizard.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.waypoints.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/modernizr-2.8.3.min.js"></script>
	<script src="js/aos.js"></script>
	<script src="js/particles.js"></script>
	<script src="js/typed.js"></script>
	<script src="js/prettify.js"></script>
	<script src="js/jquery.magnific-popup.js"></script>
	<script src="js/cookieconsent.min.js"></script>
	<script src="js/common-script.js"></script>
	<script src="js/site.js"></script>
	<!-- endinject -->
</body>
</html>
