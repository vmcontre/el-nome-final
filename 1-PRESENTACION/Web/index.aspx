﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WEB.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>El Nome - Software Gestión OT</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />

    <!-- Place favicon.ico in the root directory -->
    <link rel="ElNome" href="ElNome.png" />
    <link rel="icon" href="favicon.ico" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,700,900" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet" />

    <!-- themeforest:css -->

    <!--#include file="base.html"-->


</head>
<body>
    <!-- Making stripe menu -->
    <nav class="st-nav navbar main-nav navigation fixed-top">
        <div class="container">
            <ul class="st-nav-menu nav navbar-nav">
                <li class="st-nav-section nav-item">
                    <a href="#main" class="navbar-brand">
                        <img src="img/logo.png" alt="El Nome" class="logo logo-sticky d-block d-md-none"/>
                        <img src="img/logo-light.png" alt="El Nome" class="logo d-none d-md-block"/>
                    </a>
                </li>
                <li class="st-nav-section st-nav-primary nav-item">
                    <a class="st-root-link nav-link" href="#main">Inicio</a>
                    <a class="st-root-link nav-link" href="#NuestroSistema">Nuestro Sistema </a>
                    <a class="st-root-link nav-link" href="#NuestroEquipo">Nuestro Equipo </a>
                    <a class="st-root-link nav-link" href="#contacto">Contáctanos </a>
                </li>
                <li class="st-nav-section st-nav-secondary nav-item">
                    <a class="btn btn-rounded btn-outline mr-3 px-3" href="login.aspx" target="_blank">
                        <i class="fas fa-sign-in-alt d-none d-md-inline mr-md-0 mr-lg-2"></i>
                        <span class="d-md-none d-lg-inline">Iniciar Sesión</span>
                    </a>
                </li>
                <li class="st-nav-section st-nav-mobile nav-item">
                    <button class="st-root-link navbar-toggler" type="button">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="st-popup">
                        <div class="st-popup-container">
                            <a class="st-popup-close-button">Cerrar</a>
                            <div class="st-dropdown-content-group">
                                <h4 class="text-uppercase regular">Páginas</h4>
                                <a class="regular text-info" href="#NuestroSistema">
                                    <i class="fas fa-qrcode icon"></i>Nuestro Sistema </a>
                                <a class="regular text-primary" href="#NuestroEquipo">
                                    <i class="fas fa-users icon"></i>Nuestro Equipo </a>
                                <a class="regular text-success" href="#contacto">
                                    <i class="far fa-envelope icon"></i>Contáctanos </a>
                            </div>
                            <div class="st-dropdown-content-group bg-6 b-t">
                                <a href="login.html">Iniciar Sesión
									<i class="fas fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="st-dropdown-root">
            <div class="st-dropdown-container"></div>
        </div>
    </nav>
    <main>

        <!-- ./Page header -->
        <header id="main" class="header section overlay gradient gradient-43 alpha-8 color-1 image-background cover" style="background-image: url(img/bg/waves.jpg)">
            <div class="divider-shape">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none" class="shape-waves" style="left: 0; transform: rotate3d(0,1,0,180deg);">
                    <path class="shape-fill shape-fill-1" d="M790.5,93.1c-59.3-5.3-116.8-18-192.6-50c-29.6-12.7-76.9-31-100.5-35.9c-23.6-4.9-52.6-7.8-75.5-5.3c-10.2,1.1-22.6,1.4-50.1,7.4c-27.2,6.3-58.2,16.6-79.4,24.7c-41.3,15.9-94.9,21.9-134,22.6C72,58.2,0,25.8,0,25.8V100h1000V65.3c0,0-51.5,19.4-106.2,25.7C839.5,97,814.1,95.2,790.5,93.1z" />
                </svg>
            </div>
            <div class="container-fluid">
                <div class="row gap-y align-items-center">
                    <div class="col-lg-5 mx-auto text-center text-lg-left">
                        <h1 class="light display-4 display-xl-4 color-1">Entregamos las herramientas
							<span class="bold d-block">que necesitas para tener éxito</span>
                        </h1>
                        <p class="color-1 lead alpha-8 my-5">Todo comienza con la elección de las herramientas adecuadas, comience con un conjunto completo de bloques de diseño para lograr su próximo éxito.</p>
                        <a href="register.html" class="btn btn-1 btn-lg btn-rounded bold px-5">Pruébalo gratis</a>
                    </div>
                    <div class="col-lg-6 col-md-9 mx-md-auto mx-lg-0 pr-lg-0">
                        <div class="device-twin align-items-center mt-4 mt-lg-0">
                            <div class="browser shadow" data-aos="fade-left">
                                <img src="img/screens/dash/4.png" alt="..." />
                            </div>
                            <!--<div class="front iphone-x absolute d-none d-lg-block" data-aos="fade-right" style="left: -5.5rem; bottom: -3.5rem;">
								<div class="screen">
									<img src="img/screens/app/1.png" alt="...">
								</div>
								<div class="notch"></div>
							</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- ./Features - grid -->
        <section id="NuestroSistema" class="section edge bottom-right bg-1">
            <div class="container pb-5">
                <div class="section-heading mb-6 text-center">
                    <p class="bold text-uppercase mb-0 badge badge-4">Características de El Nome</p>
                    <h2>¿Qué puede hacer El Nome por ti?</h2>
                </div>
                <div class="row gap-y text-center text-md-left">
                    <div class="col-md-4 py-4 rounded shadow-hover">
                        <i class="pe pe-7s-door-lock pe-3x color-4 mb-2"></i>
                        <h5 class="bold">Seguridad Incluida</h5>
                        <p class="color-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dolores ea fugiat nesciunt quisquam. Assumenda dolore error nulla pariatur voluptatem?</p>
                    </div>
                    <div class="col-md-4 py-4 rounded shadow-hover">
                        <i class="pe pe-7s-paint-bucket pe-3x color-4 mb-2"></i>
                        <h5 class="bold">Easily theme-able</h5>
                        <p class="color-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dolores ea fugiat nesciunt quisquam. Assumenda dolore error nulla pariatur voluptatem?</p>
                    </div>
                    <div class="col-md-4 py-4 rounded shadow-hover">
                        <i class="pe pe-7s-plugin pe-3x color-4 mb-2"></i>
                        <h5 class="bold">Plugins Incluidos</h5>
                        <p class="color-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dolores ea fugiat nesciunt quisquam. Assumenda dolore error nulla pariatur voluptatem?</p>
                    </div>
                    <div class="col-md-4 py-4 rounded shadow-hover">
                        <i class="pe pe-7s-tools pe-3x color-4 mb-2"></i>
                        <h5 class="bold">Herramientas Profesionales</h5>
                        <p class="color-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dolores ea fugiat nesciunt quisquam. Assumenda dolore error nulla pariatur voluptatem?</p>
                    </div>
                    <div class="col-md-4 py-4 rounded shadow-hover">
                        <i class="pe pe-7s-target pe-3x color-4 mb-2"></i>
                        <h5 class="bold">Contenido Listo Para usar</h5>
                        <p class="color-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dolores ea fugiat nesciunt quisquam. Assumenda dolore error nulla pariatur voluptatem?</p>
                    </div>
                    <div class="col-md-4 py-4 rounded shadow-hover">
                        <i class="pe pe-7s-graph1 pe-3x color-4 mb-2"></i>
                        <h5 class="bold">Anlisis de gran alcance</h5>
                        <p class="color-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dolores ea fugiat nesciunt quisquam. Assumenda dolore error nulla pariatur voluptatem?</p>
                    </div>
                </div>
            </div>
        </section>

        <!-- User Reviews -->
        <section id="NuestroEquipo" class="section singl-testimonial">
            <div class="container pt-8 bring-to-front">
                <div class="section-heading mb-6 text-center">
                    <p class="bold text-uppercase mb-0 badge badge-4">El Equipo</p>
                    <h2>Conoce Nuestro Equipo!!!</h2>
                </div>
                <div class="swiper-container pb-0 pb-lg-8" data-sw-nav-arrows=".reviews-nav">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="row gap-y align-items-center">
                                <div class="col-lg-6">
                                    <figure class="testimonial-img ml-md-auto">
                                        <img src="img/v6/reviews/1.jpg" class="img-responsive rounded shadow-lg" alt="..." />
                                    </figure>
                                </div>
                                <div class="col-lg-6 ml-md-auto">
                                    <div class="user-review text-center italic bg-3 color-1 rounded shadow-lg py-5 px-4 px-lg-6">
                                        <blockquote class="regular py-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet aspernatur, autem deserunt distinctio dolores eius, exercitationem facilis inventore.</blockquote>
                                        <div class="author mt-4">
                                            <p class="small">
                                                <span class="bold color-5">Valeria Contreras,</span> Web Developer
                                            </p>
                                        </div>
                                        <div class="shape-wrapper" data-aos="fade-up">
                                            <svg class="svg-review-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#8B41EB"></path>
                                            </svg>
                                            <svg class="svg-review-bottom back" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#DC92FF"></path>
                                            </svg>
                                            <svg class="svg-review-bottom back left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#A45AFF"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="row gap-y align-items-center">
                                <div class="col-lg-6">
                                    <figure class="testimonial-img ml-md-auto">
                                        <img src="img/v6/reviews/2.jpg" class="img-responsive rounded shadow-lg" alt="..."/>
                                    </figure>
                                </div>
                                <div class="col-lg-6 ml-md-auto">
                                    <div class="user-review text-center italic bg-3 color-1 rounded shadow-lg py-5 px-4 px-lg-6">
                                        <blockquote class="regular py-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet aspernatur, autem deserunt distinctio dolores eius, exercitationem facilis inventore.</blockquote>
                                        <div class="author mt-4">
                                            <p class="small">
                                                <span class="bold color-5">Manuel Romero,</span> Web Developer
                                            </p>
                                        </div>
                                        <div class="shape-wrapper" data-aos="fade-up">
                                            <svg class="svg-review-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#8B41EB"></path>
                                            </svg>
                                            <svg class="svg-review-bottom back" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#DC92FF"></path>
                                            </svg>
                                            <svg class="svg-review-bottom back left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#A45AFF"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="row gap-y align-items-center">
                                <div class="col-lg-6">
                                    <figure class="testimonial-img ml-md-auto">
                                        <img src="img/v6/reviews/3.jpg" class="img-responsive rounded shadow-lg" alt="..." />
                                    </figure>
                                </div>
                                <div class="col-lg-6 ml-md-auto">
                                    <div class="user-review text-center italic bg-3 color-1 rounded shadow-lg py-5 px-4 px-lg-6">
                                        <blockquote class="regular py-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet aspernatur, autem deserunt distinctio dolores eius, exercitationem facilis inventore.</blockquote>
                                        <div class="author mt-4">
                                            <p class="small">
                                                <span class="bold color-5">Rodrigo Mendoza,</span> Web Developer
                                            </p>
                                        </div>
                                        <div class="shape-wrapper" data-aos="fade-up">
                                            <svg class="svg-review-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#8B41EB"></path>
                                            </svg>
                                            <svg class="svg-review-bottom back" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#DC92FF"></path>
                                            </svg>
                                            <svg class="svg-review-bottom back left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                                                <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#A45AFF"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add Arrows -->
                    <div class="reviews-navigation">
                        <div class="reviews-nav reviews-nav-prev btn btn-6 btn-rounded shadow-box shadow-hover">
                            <i class="reviews-nav-icon fas fa-long-arrow-alt-left"></i>
                        </div>
                        <div class="reviews-nav reviews-nav-next btn btn-6 btn-rounded shadow-box shadow-hover">
                            <i class="reviews-nav-icon fas fa-long-arrow-alt-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ./CTA - Create Account -->
        <section id="contacto" class="section bg-1 edge b-b">
            <div class="container pt-5">
                <div class="d-flex align-items-center flex-column flex-md-row">
                    <div class="text-center text-md-left">
                        <p class="light mb-0 accent lead">¿Listo para comenzar?</p>
                        <h2 class="mt-0">Crea una cuenta ahora</h2>
                    </div>
                    <a href="register.html" class="btn btn-accent btn-rounded mt-3 mt-md-0 ml-md-auto">Crear una cuenta de El Nome</a>
                </div>
            </div>
        </section>
    </main>

    <!-- ./Footer - Stay in Touch -->
    <footer class="site-footer section text-center">
        <div class="container pb-4">
            <div class="font-md regular">¿Necesitas Asesoria?</div>
            <p class="color-2 mb-5">Registra el correo y te contactaremos a la brevedad posible</p>
            <div class="row">
                <div class="col-12 col-md-6 mx-auto overflow-hidden">
                    <form action="srv/register.php" class="form" data-response-message-animation="slide-in-left">
                        <div class="input-group">
                            <input type="email" name="Subscribe[email]" class="form-control rounded-circle-left" placeholder="Introduce tu Email" required="required" />
                            <div class="input-group-append">
                                <button class="btn btn-rounded btn-accent" type="submit">Enviar</button>
                            </div>
                        </div>
                    </form>
                    <div class="response-message">
                        <i class="fas fa-envelope font-lg"></i>
                        <p class="font-md m-0">Consultar su correo electrónico</p>
                        <p class="response">Te enviamos un Email con informacion relevante.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- ./Footer - Simple -->
    <footer class="site-footer section">
        <div class="container py-4">
            <hr class="mt-5" />
            <div class="row align-items-center">
                <div class="col-md-5 text-center text-md-left">
                    <p class="mt-2 mb-0 color-2 small">© 2019 - Todos los derechos reservados</p>
                </div>
                <div class="col-md-2 text-center">
                    <a href="#main">
                        <img src="img/logo.png" alt="" class="logo" />
                    </a>
                </div>
                <div class="col-md-5 text-center text-md-right">
                    <nav class="nav justify-content-center justify-content-md-end">
                        <a href="https://www.facebook.com/" class="btn btn-circle btn-sm btn-2 mr-3 op-4" target="_blank">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/" class="btn btn-circle btn-sm btn-2 mr-3 op-4" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="https://www.linkedin.com/" class="btn btn-circle btn-sm btn-2 op-4" target="_blank">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </nav>
                </div>
            </div>
        </div>
    </footer>

    <!-- themeforest:js -->
    <script src="js/01.cookie-consent-util.js"></script>
    <script src="js/02.1.cookie-consent-themes.js"></script>
    <script src="js/02.2.cookie-consent-custom-css.js"></script>
    <script src="js/02.3.cookie-consent-informational.js"></script>
    <script src="js/02.4.cookie-consent-opt-out.js"></script>
    <script src="js/02.5.cookie-consent-opt-in.js"></script>
    <script src="js/02.6.cookie-consent-location.js"></script>

    <script src="js/jquery.js"></script>
    <script src="js/jquery.animatebar.js"></script>
    <script src="js/odometer.min.js"></script>
    <script src="js/simplebar.js"></script>
    <script src="js/swiper.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/jquery.smartWizard.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.waypoints.js"></script>
    <script src="js/jquery.counterup.js"></script>
    <script src="js/modernizr-2.8.3.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/particles.js"></script>
    <script src="js/typed.js"></script>
    <script src="js/prettify.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <script src="js/cookieconsent.min.js"></script>
    <script src="js/common-script.js"></script>
    <script src="js/forms.js"></script>
    <script src="js/site.js"></script>
    <script src="js/stripe-menu.js"></script>
    <!-- endinject -->
</body>
</html>
