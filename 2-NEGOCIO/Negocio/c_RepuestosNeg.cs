﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio {
    public class RepuestosNeg {
        private RepuestosDAL repuestosDAL = new RepuestosDAL();

        public ResultadoDTO CrearRepuesto(repuestos dto) {
            return repuestosDAL.CrearRepuesto(dto);
        }
        public List<ItemRepuestos> ListarRepuestos() {
            return repuestosDAL.ListarRepuestos();
        }
        public repuestos BuscarRepuesto(int id) {
            return repuestosDAL.BuscarRepuesto(id);
        }
        public ResultadoDTO ActualizarStockRepuesto(int idRepuesto, int stockRepuesto ,String nuevoNombreRep) {
            return repuestosDAL.ActualizarStockRepuesto(idRepuesto, stockRepuesto, nuevoNombreRep);
        }

        public ResultadoDTO BorrarRepuesto(int idRep) {
            return repuestosDAL.BorrarRepuesto(idRep);
        }

        public List<ItemRepuestos> ListarRepuestosConStock() {
            return repuestosDAL.ListarRepuestosConStock();
        }
    }
}
