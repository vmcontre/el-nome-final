﻿using Datos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio {
    public class CargosNeg {
        private CargosDAL cargosDAL = new CargosDAL();

        public ResultadoDTO CrearCargo(cargos dto) {
            return cargosDAL.CrearCargo(dto);
        }
        public List<ItemCargos> ListarCargos() {
            return cargosDAL.ListarCargos();
        }
        public ResultadoDTO ModificarCargos(int idCargo, String descrCargo) {
            return cargosDAL.ModificarCargos(idCargo, descrCargo);
        }
        public cargos BuscarCargos(int idCargo) {
            return cargosDAL.BuscarCargos(idCargo);
        }

        public ResultadoDTO EliminarCargo(int idCargo) {
            return cargosDAL.EliminarCargo(idCargo);
        }
    }
}
