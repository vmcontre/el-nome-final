﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio {
    public class OrdenTrabajoNeg {
        private OrdenTrabajoDAL ordenTrabajoDAL = new OrdenTrabajoDAL();

        public ResultadoDTO CrearOT(ordenTrabajo dto) {
            return ordenTrabajoDAL.CrearOT(dto);
        }
        public List<ItemOT> ListarOT() {
            return ordenTrabajoDAL.ListarOT();
        }
        public ordenTrabajo BuscarOT(int id) {
            return ordenTrabajoDAL.BuscarOT(id);
        }
        public ResultadoDTO CambiarEstadoOT(int id, int idStatus) {
            return ordenTrabajoDAL.CambiarEstadoOT(id, idStatus);
        }
        public OrdenTrabajoDTO BuscarOrdenTrabajo(int idOT) {
            return ordenTrabajoDAL.BuscarOrdenTrabajo(idOT);
        }
        public List<OrdenTrabajoDTO> ListarOrdenTrabajo() {
            return ordenTrabajoDAL.ListarOrdenTrabajo();
        }

        public List<Int32> ListarNroOT() {
            return ordenTrabajoDAL.ListarNroOT();
        }
    }
}
