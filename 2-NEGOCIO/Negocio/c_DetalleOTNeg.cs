﻿using Datos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio {
    public class DetalleOTNeg {
        private DetalleOTDAL detalleOTDAL = new DetalleOTDAL();

        public ResultadoDTO CrearDetalleOT(detalleOT dto) {
            return detalleOTDAL.CrearDetalleOT(dto);
        }
        public List<ItemDetalleOT> ListarDOT(int id) {
            return detalleOTDAL.ListarDOT(id);
        }
        public detalleOT BuscarDetOT(int id) {
            return detalleOTDAL.BuscarDetOT(id);
        }
        public ResultadoDTO ModificarDetOT(int id, int idTec, int idRep, int idFalla) {
            return detalleOTDAL.ModificarDetOT(id, idTec, idRep, idFalla);
        }

        public DetalleOTDTO BuscarDetalleOT_DTO(int id) {
            return detalleOTDAL.BuscarDetalleOT_DTO(id);
        }

        public List<DetalleOTDTO> ListarDOT_DTO(int id) {
            return detalleOTDAL.ListarDOT_DTO(id);
        }
    }
}
