﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio {
    public class StatusOTNeg {
        private StatusOTDAL statusOTDAL = new StatusOTDAL();

        public List<ItemStatusOT> listarStatusOT() {
            return statusOTDAL.listarStatusOT();
        }

        public String BuscarStatusOT(int id) {
            return statusOTDAL.BuscarStatusOT(id);
        }

        public int BuscarIDStatusOT(String descrStatus) {
            return statusOTDAL.BuscarIDStatusOT(descrStatus);
        }

    }
}
