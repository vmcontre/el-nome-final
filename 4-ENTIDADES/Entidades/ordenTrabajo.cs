//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class ordenTrabajo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ordenTrabajo()
        {
            this.detalleOT = new HashSet<detalleOT>();
        }
    
        public int idOT { get; set; }
        public System.DateTime fechaCreacion { get; set; }
        public int idMaquina { get; set; }
        public int idCreador { get; set; }
        public int idEncargado { get; set; }
        public int idStatusOT { get; set; }
        public Nullable<System.DateTime> fechaCierre { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<detalleOT> detalleOT { get; set; }
        public virtual maquinas maquinas { get; set; }
        public virtual personas personas { get; set; }
        public virtual personas personas1 { get; set; }
        public virtual statusOT statusOT { get; set; }
    }
}
