﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades {
    public class OrdenTrabajoDTO {
        public int idOT { get; set; }
        public List<DetalleOTDTO> listaDetalles { get; set; }
        public String nombreMaquina { get; set; }
        public String nombreCreador { get; set; }
        public String nombreEncargado { get; set; }
        public String status { get; set; }
        public DateTime? fechaCierre { get; set; }
        public DateTime fechaCreacion { get; set; }
        public int idStatusOT { get; set; }

    }

    public class DetalleOTDTO {
        public int idOT { get; set; }
        public int idDOT { get; set; }
        public String nombreTecnico { get; set; }
        public String nombreFalla { get; set; }
        public String nombreRepuesto { get; set; }
        public DateTime fecha { get; set; }
    }
}
